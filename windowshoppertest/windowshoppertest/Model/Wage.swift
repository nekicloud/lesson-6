//
//  File.swift
//  windowshoppertest
//
//  Created by luka on 24/12/2018.
//  Copyright © 2018 luka. All rights reserved.
//

import Foundation

class Wage {
    class func getHours(forWage wage: Double, andPrice price: Double) -> Int{
        return Int(ceil(price / wage))
    }
}
